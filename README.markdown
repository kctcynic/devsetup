What this is
============
Shell scripts to build a development environment for Ogre and LibRocket on top of Ubuntu 12.04. It includes the current LibRocket deb and pulls the rest from the net.


when checking out, use:
    git clone https://bitbucket.org/kctcynic/devsetup.git ~/Software


Also, setup git author info

    git config --global user.name "Your Name"
    git config --global user.email you@example.com


To Do
-----
Grab the debs that are installed and include them here so we can outlast even the lts timeframe if need be.
