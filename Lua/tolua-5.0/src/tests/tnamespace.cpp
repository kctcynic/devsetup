extern "C" {
#include "lualib.h"
#include "lauxlib.h"
}

#include "tnamespace.h"

int A::a = 1;
int A::B::b = 2;
int A::B::C::c = 3;
A::Order D::d = A::FIRST;
A::B::Bool D::e = A::B::FALSE;

int main ()
{
	int  tolua_tnamespace_open (lua_State*);

	lua_State* L = lua_open();
	luaopen_base(L);
	tolua_tnamespace_open(L);

	lua_dofile(L,"tnamespace.lua");

	lua_close(L);
	return 0;
}

