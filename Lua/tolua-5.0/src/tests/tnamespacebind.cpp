/*
** Lua binding: tnamespace
** Generated automatically by tolua 5.0 on Tue May  8 17:56:13 2012.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_tnamespace_open (lua_State* tolua_S);

#include "tnamespace.h"

/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"A::Order");
 tolua_usertype(tolua_S,"A::B::Bool");
}

/* get function: A::a */
static int tolua_get_A_a(lua_State* tolua_S)
{
 tolua_pushnumber(tolua_S,(lua_Number)A::a);
 return 1;
}

/* set function: A::a */
static int tolua_set_A_a(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  A::a = ((int)  tolua_tonumber(tolua_S,2,0));
 return 0;
}

/* get function: A::B::b */
static int tolua_get_B_b(lua_State* tolua_S)
{
 tolua_pushnumber(tolua_S,(lua_Number)A::B::b);
 return 1;
}

/* set function: A::B::b */
static int tolua_set_B_b(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  A::B::b = ((int)  tolua_tonumber(tolua_S,2,0));
 return 0;
}

/* get function: A::B::C::c */
static int tolua_get_C_c(lua_State* tolua_S)
{
 tolua_pushnumber(tolua_S,(lua_Number)A::B::C::c);
 return 1;
}

/* set function: A::B::C::c */
static int tolua_set_C_c(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  A::B::C::c = ((int)  tolua_tonumber(tolua_S,2,0));
 return 0;
}

/* get function: D::d */
static int tolua_get_D_d(lua_State* tolua_S)
{
 tolua_pushnumber(tolua_S,(lua_Number)D::d);
 return 1;
}

/* set function: D::d */
static int tolua_set_D_d(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  D::d = ((A::Order) (int)  tolua_tonumber(tolua_S,2,0));
 return 0;
}

/* get function: D::e */
static int tolua_get_D_e(lua_State* tolua_S)
{
 tolua_pushnumber(tolua_S,(lua_Number)D::e);
 return 1;
}

/* set function: D::e */
static int tolua_set_D_e(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  D::e = ((A::B::Bool) (int)  tolua_tonumber(tolua_S,2,0));
 return 0;
}

/* Open function */
TOLUA_API int tolua_tnamespace_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_module(tolua_S,"A",1);
 tolua_beginmodule(tolua_S,"A");
 tolua_constant(tolua_S,"FIRST",A::FIRST);
 tolua_constant(tolua_S,"SECOND",A::SECOND);
 tolua_variable(tolua_S,"a",tolua_get_A_a,tolua_set_A_a);
 tolua_module(tolua_S,"B",1);
 tolua_beginmodule(tolua_S,"B");
 tolua_constant(tolua_S,"FALSE",A::B::FALSE);
 tolua_constant(tolua_S,"TRUE",A::B::TRUE);
 tolua_variable(tolua_S,"b",tolua_get_B_b,tolua_set_B_b);
 tolua_module(tolua_S,"C",1);
 tolua_beginmodule(tolua_S,"C");
 tolua_variable(tolua_S,"c",tolua_get_C_c,tolua_set_C_c);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 tolua_module(tolua_S,"D",1);
 tolua_beginmodule(tolua_S,"D");
 tolua_variable(tolua_S,"d",tolua_get_D_d,tolua_set_D_d);
 tolua_variable(tolua_S,"e",tolua_get_D_e,tolua_set_D_e);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
