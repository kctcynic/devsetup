/*
** Lua binding: tconstant
** Generated automatically by tolua 5.0 on Tue May  8 17:56:14 2012.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_tconstant_open (lua_State* tolua_S);

#include "tconstant.h"

/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"A");
}

/* Open function */
TOLUA_API int tolua_tconstant_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_constant(tolua_S,"FIRST",G_FIRST);
 tolua_constant(tolua_S,"SECOND",G_SECOND);
 tolua_constant(tolua_S,"ONE",G_ONE);
 tolua_constant(tolua_S,"TWO",G_TWO);
 tolua_module(tolua_S,"M",0);
 tolua_beginmodule(tolua_S,"M");
 tolua_constant(tolua_S,"FIRST",M_FIRST);
 tolua_constant(tolua_S,"SECOND",M_SECOND);
 tolua_constant(tolua_S,"ONE",M_ONE);
 tolua_constant(tolua_S,"TWO",M_TWO);
 tolua_endmodule(tolua_S);
 tolua_cclass(tolua_S,"A","A","",NULL);
 tolua_beginmodule(tolua_S,"A");
 tolua_constant(tolua_S,"FIRST",FIRST);
 tolua_constant(tolua_S,"SECOND",SECOND);
 tolua_constant(tolua_S,"ONE",A::ONE);
 tolua_constant(tolua_S,"TWO",A::TWO);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
