extern "C"
{
#include "lualib.h"
#include "lauxlib.h"
}

#include "tconstant.h"


int main (void)
{
	int  tolua_tconstant_open (lua_State*);
	lua_State* L = lua_open();
	luaopen_base(L);
	tolua_tconstant_open(L);

	lua_dofile(L,"tconstant.lua");

	lua_close(L);
	return 0;
}

