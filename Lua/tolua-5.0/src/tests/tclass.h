#include <stdio.h>

class Tst_Dummy
{
};

class Tst_A
{
public:
	static Tst_A* last;
	Tst_A () {last = this;}
	virtual char* a () { return "A"; } 
	class Tst_AA
	{
 public:
		Tst_AA () {}
		~Tst_AA () { }
		char* aa () { return "AA"; }
	};
	class Tst_BB : public Tst_AA
	{
 public:
		Tst_BB () {}
		~Tst_BB () {}
		Tst_AA* Base () { return this; }
	};
};

class Tst_B : public Tst_A
{
public:
	static Tst_B* last;
	Tst_B () {last = this;}
	virtual char* b () { return "B"; } 
};

class Tst_C : public Tst_B
{
	int i;
public:
	static Tst_C* last;
	Tst_C (int n) : i(n) {last = this;}
 virtual ~Tst_C () { }
	virtual char* c () { return "C"; } 
};

inline Tst_A::Tst_AA* Tst_create_aa ()
{
	return new Tst_A::Tst_AA();
}

inline bool Tst_is_aa (Tst_A::Tst_AA* obj)
{
	return true;
}

class Class {
public:
	enum Enum {
		ONE,
		TWO
	};
};

class Class2 {
public:
	Class::Enum enu;
	Class2 (Class::Enum e) {enu = e; }
};

class V {
 int m_n;
public:
	V (int n) : m_n(n) {}
	~V () {}
	int get () const {
		return m_n;
	}
};

class C {
	V m_v;
public:
	C (int n) : m_v(n) {}
	~C () {}
	const V& getV () const {
		return m_v;
	}
	V copyV () const {
		return m_v;
	}
};
