/*
** Lua binding: tclass
** Generated automatically by tolua 5.0 on Tue May  8 17:56:13 2012.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_tclass_open (lua_State* tolua_S);

#include "tclass.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_Tst_C (lua_State* tolua_S)
{
 Tst_C* self = (Tst_C*) tolua_tousertype(tolua_S,1,0);
 tolua_release(tolua_S,self);
 delete self;
 return 0;
}

static int tolua_collect_Tst_A_Tst_BB (lua_State* tolua_S)
{
 Tst_A::Tst_BB* self = (Tst_A::Tst_BB*) tolua_tousertype(tolua_S,1,0);
 tolua_release(tolua_S,self);
 delete self;
 return 0;
}

static int tolua_collect_Tst_A_Tst_AA (lua_State* tolua_S)
{
 Tst_A::Tst_AA* self = (Tst_A::Tst_AA*) tolua_tousertype(tolua_S,1,0);
 tolua_release(tolua_S,self);
 delete self;
 return 0;
}

static int tolua_collect_C (lua_State* tolua_S)
{
 C* self = (C*) tolua_tousertype(tolua_S,1,0);
 tolua_release(tolua_S,self);
 delete self;
 return 0;
}

static int tolua_collect_V (lua_State* tolua_S)
{
 V* self = (V*) tolua_tousertype(tolua_S,1,0);
 tolua_release(tolua_S,self);
 delete self;
 return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Tst_C");
 tolua_usertype(tolua_S,"Tst_A::Tst_BB");
 tolua_usertype(tolua_S,"Tst_A");
 tolua_usertype(tolua_S,"Class");
 tolua_usertype(tolua_S,"V");
 tolua_usertype(tolua_S,"Class2");
 tolua_usertype(tolua_S,"Class::Enum");
 tolua_usertype(tolua_S,"C");
 tolua_usertype(tolua_S,"Tst_Dummy");
 tolua_usertype(tolua_S,"Tst_A::Tst_AA");
 tolua_usertype(tolua_S,"Tst_B");
}

/* get function: last of class  Tst_A */
static int tolua_get_Tst_A_Tst_A_last_ptr(lua_State* tolua_S)
{
 tolua_pushusertype(tolua_S,(void*)Tst_A::last,"Tst_A");
 return 1;
}

/* set function: last of class  Tst_A */
static int tolua_set_Tst_A_Tst_A_last_ptr(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!tolua_isusertype(tolua_S,2,"Tst_A",0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  Tst_A::last = ((Tst_A*)  tolua_tousertype(tolua_S,2,0));
 return 0;
}

/* method: new of class  Tst_A */
static int tolua_tclass_A_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Tst_A",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
 {
  Tst_A* tolua_ret = (Tst_A*)  new Tst_A();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Tst_A");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: a of class  Tst_A */
static int tolua_tclass_A_a00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Tst_A",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Tst_A* self = (Tst_A*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'a'",NULL);
#endif
 {
  char* tolua_ret = (char*)  self->a();
 tolua_pushstring(tolua_S,(const char*)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'a'.",&tolua_err);
 return 0;
#endif
}

/* method: new of class  Tst_AA */
static int tolua_tclass_A_AA_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Tst_A::Tst_AA",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
 {
  Tst_A::Tst_AA* tolua_ret = (Tst_A::Tst_AA*)  new Tst_A::Tst_AA();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Tst_A::Tst_AA");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: delete of class  Tst_AA */
static int tolua_tclass_A_AA_delete00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Tst_A::Tst_AA",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Tst_A::Tst_AA* self = (Tst_A::Tst_AA*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'delete'",NULL);
#endif
 tolua_release(tolua_S,self);
 delete self;
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'delete'.",&tolua_err);
 return 0;
#endif
}

/* method: aa of class  Tst_AA */
static int tolua_tclass_A_AA_aa00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Tst_A::Tst_AA",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Tst_A::Tst_AA* self = (Tst_A::Tst_AA*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'aa'",NULL);
#endif
 {
  char* tolua_ret = (char*)  self->aa();
 tolua_pushstring(tolua_S,(const char*)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'aa'.",&tolua_err);
 return 0;
#endif
}

/* method: new of class  Tst_BB */
static int tolua_tclass_A_BB_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Tst_A::Tst_BB",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
 {
  Tst_A::Tst_BB* tolua_ret = (Tst_A::Tst_BB*)  new Tst_A::Tst_BB();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Tst_A::Tst_BB");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: delete of class  Tst_BB */
static int tolua_tclass_A_BB_delete00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Tst_A::Tst_BB",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Tst_A::Tst_BB* self = (Tst_A::Tst_BB*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'delete'",NULL);
#endif
 tolua_release(tolua_S,self);
 delete self;
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'delete'.",&tolua_err);
 return 0;
#endif
}

/* method: Base of class  Tst_BB */
static int tolua_tclass_A_BB_Base00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Tst_A::Tst_BB",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Tst_A::Tst_BB* self = (Tst_A::Tst_BB*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'Base'",NULL);
#endif
 {
  Tst_A::Tst_AA* tolua_ret = (Tst_A::Tst_AA*)  self->Base();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Tst_A::Tst_AA");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'Base'.",&tolua_err);
 return 0;
#endif
}

/* get function: last of class  Tst_B */
static int tolua_get_Tst_B_Tst_B_last_ptr(lua_State* tolua_S)
{
 tolua_pushusertype(tolua_S,(void*)Tst_B::last,"Tst_B");
 return 1;
}

/* set function: last of class  Tst_B */
static int tolua_set_Tst_B_Tst_B_last_ptr(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!tolua_isusertype(tolua_S,2,"Tst_B",0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  Tst_B::last = ((Tst_B*)  tolua_tousertype(tolua_S,2,0));
 return 0;
}

/* method: new of class  Tst_B */
static int tolua_tclass_B_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Tst_B",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
 {
  Tst_B* tolua_ret = (Tst_B*)  new Tst_B();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Tst_B");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: b of class  Tst_B */
static int tolua_tclass_B_b00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Tst_B",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Tst_B* self = (Tst_B*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'b'",NULL);
#endif
 {
  char* tolua_ret = (char*)  self->b();
 tolua_pushstring(tolua_S,(const char*)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'b'.",&tolua_err);
 return 0;
#endif
}

/* get function: last of class  Tst_C */
static int tolua_get_Tst_C_Tst_C_last_ptr(lua_State* tolua_S)
{
 tolua_pushusertype(tolua_S,(void*)Tst_C::last,"Tst_C");
 return 1;
}

/* set function: last of class  Tst_C */
static int tolua_set_Tst_C_Tst_C_last_ptr(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!tolua_isusertype(tolua_S,2,"Tst_C",0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  Tst_C::last = ((Tst_C*)  tolua_tousertype(tolua_S,2,0));
 return 0;
}

/* method: new of class  Tst_C */
static int tolua_tclass_luaC_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Tst_C",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  int n = ((int)  tolua_tonumber(tolua_S,2,0));
 {
  Tst_C* tolua_ret = (Tst_C*)  new Tst_C(n);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Tst_C");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: delete of class  Tst_C */
static int tolua_tclass_luaC_delete00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Tst_C",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Tst_C* self = (Tst_C*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'delete'",NULL);
#endif
 tolua_release(tolua_S,self);
 delete self;
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'delete'.",&tolua_err);
 return 0;
#endif
}

/* method: c of class  Tst_C */
static int tolua_tclass_luaC_c00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Tst_C",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Tst_C* self = (Tst_C*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'c'",NULL);
#endif
 {
  char* tolua_ret = (char*)  self->c();
 tolua_pushstring(tolua_S,(const char*)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'c'.",&tolua_err);
 return 0;
#endif
}

/* function: Tst_create_aa */
static int tolua_tclass_create_aa00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isnoobj(tolua_S,1,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
 {
  Tst_A::Tst_AA* tolua_ret = (Tst_A::Tst_AA*)  Tst_create_aa();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Tst_A::Tst_AA");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'create_aa'.",&tolua_err);
 return 0;
#endif
}

/* function: Tst_is_aa */
static int tolua_tclass_is_aa00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Tst_A::Tst_AA",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Tst_A::Tst_AA* obj = ((Tst_A::Tst_AA*)  tolua_tousertype(tolua_S,1,0));
 {
  bool tolua_ret = (bool)  Tst_is_aa(obj);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'is_aa'.",&tolua_err);
 return 0;
#endif
}

/* get function: enu of class  Class2 */
static int tolua_get_Class2_Class2_enu(lua_State* tolua_S)
{
  Class2* self = (Class2*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'enu'",NULL);
#endif
 tolua_pushnumber(tolua_S,(lua_Number)self->enu);
 return 1;
}

/* set function: enu of class  Class2 */
static int tolua_set_Class2_Class2_enu(lua_State* tolua_S)
{
  Class2* self = (Class2*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'enu'",NULL);
 if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->enu = ((Class::Enum) (int)  tolua_tonumber(tolua_S,2,0));
 return 0;
}

/* method: new of class  Class2 */
static int tolua_tclass_Class2_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Class2",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Class::Enum e = ((Class::Enum)  tolua_tonumber(tolua_S,2,0));
 {
  Class2* tolua_ret = (Class2*)  new Class2(e);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Class2");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: new of class  V */
static int tolua_tclass_V_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"V",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  int n = ((int)  tolua_tonumber(tolua_S,2,0));
 {
  V* tolua_ret = (V*)  new V(n);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"V");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: get of class  V */
static int tolua_tclass_V_get00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"const V",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  const V* self = (const V*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'get'",NULL);
#endif
 {
  int tolua_ret = (int)  self->get();
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'get'.",&tolua_err);
 return 0;
#endif
}

/* method: new of class  C */
static int tolua_tclass_C_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"C",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  int n = ((int)  tolua_tonumber(tolua_S,2,0));
 {
  C* tolua_ret = (C*)  new C(n);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"C");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: delete of class  C */
static int tolua_tclass_C_delete00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"C",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  C* self = (C*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'delete'",NULL);
#endif
 tolua_release(tolua_S,self);
 delete self;
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'delete'.",&tolua_err);
 return 0;
#endif
}

/* method: getV of class  C */
static int tolua_tclass_C_getV00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"const C",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  const C* self = (const C*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getV'",NULL);
#endif
 {
  const V& tolua_ret = (const V&)  self->getV();
 tolua_pushusertype(tolua_S,(void*)&tolua_ret,"const V");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getV'.",&tolua_err);
 return 0;
#endif
}

/* method: copyV of class  C */
static int tolua_tclass_C_copyV00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"const C",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  const C* self = (const C*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'copyV'",NULL);
#endif
 {
  V tolua_ret =  self->copyV();
 {
#ifdef __cplusplus
 void* tolua_obj = new V(tolua_ret);
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,tolua_collect_V),"V");
#else
 void* tolua_obj = tolua_copy(tolua_S,(void*)&tolua_ret,sizeof(V));
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,NULL),"V");
#endif
 }
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'copyV'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_tclass_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_cclass(tolua_S,"Dummy","Tst_Dummy","",NULL);
 tolua_beginmodule(tolua_S,"Dummy");
 tolua_endmodule(tolua_S);
 tolua_cclass(tolua_S,"A","Tst_A","",NULL);
 tolua_beginmodule(tolua_S,"A");
 tolua_variable(tolua_S,"last",tolua_get_Tst_A_Tst_A_last_ptr,tolua_set_Tst_A_Tst_A_last_ptr);
 tolua_function(tolua_S,"new",tolua_tclass_A_new00);
 tolua_function(tolua_S,"a",tolua_tclass_A_a00);
#ifdef __cplusplus
 tolua_cclass(tolua_S,"AA","Tst_A::Tst_AA","",tolua_collect_Tst_A_Tst_AA);
#else
 tolua_cclass(tolua_S,"AA","Tst_A::Tst_AA","",NULL);
#endif
 tolua_beginmodule(tolua_S,"AA");
 tolua_function(tolua_S,"new",tolua_tclass_A_AA_new00);
 tolua_function(tolua_S,"delete",tolua_tclass_A_AA_delete00);
 tolua_function(tolua_S,"aa",tolua_tclass_A_AA_aa00);
 tolua_endmodule(tolua_S);
#ifdef __cplusplus
 tolua_cclass(tolua_S,"BB","Tst_A::Tst_BB","Tst_A::Tst_AA",tolua_collect_Tst_A_Tst_BB);
#else
 tolua_cclass(tolua_S,"BB","Tst_A::Tst_BB","Tst_A::Tst_AA",NULL);
#endif
 tolua_beginmodule(tolua_S,"BB");
 tolua_function(tolua_S,"new",tolua_tclass_A_BB_new00);
 tolua_function(tolua_S,"delete",tolua_tclass_A_BB_delete00);
 tolua_function(tolua_S,"Base",tolua_tclass_A_BB_Base00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 tolua_cclass(tolua_S,"B","Tst_B","Tst_A",NULL);
 tolua_beginmodule(tolua_S,"B");
 tolua_variable(tolua_S,"last",tolua_get_Tst_B_Tst_B_last_ptr,tolua_set_Tst_B_Tst_B_last_ptr);
 tolua_function(tolua_S,"new",tolua_tclass_B_new00);
 tolua_function(tolua_S,"b",tolua_tclass_B_b00);
 tolua_endmodule(tolua_S);
#ifdef __cplusplus
 tolua_cclass(tolua_S,"luaC","Tst_C","Tst_B",tolua_collect_Tst_C);
#else
 tolua_cclass(tolua_S,"luaC","Tst_C","Tst_B",NULL);
#endif
 tolua_beginmodule(tolua_S,"luaC");
 tolua_variable(tolua_S,"last",tolua_get_Tst_C_Tst_C_last_ptr,tolua_set_Tst_C_Tst_C_last_ptr);
 tolua_function(tolua_S,"new",tolua_tclass_luaC_new00);
 tolua_function(tolua_S,"delete",tolua_tclass_luaC_delete00);
 tolua_function(tolua_S,"c",tolua_tclass_luaC_c00);
 tolua_endmodule(tolua_S);
 tolua_function(tolua_S,"create_aa",tolua_tclass_create_aa00);
 tolua_function(tolua_S,"is_aa",tolua_tclass_is_aa00);
 tolua_cclass(tolua_S,"Class","Class","",NULL);
 tolua_beginmodule(tolua_S,"Class");
 tolua_constant(tolua_S,"ONE",Class::ONE);
 tolua_constant(tolua_S,"TWO",Class::TWO);
 tolua_endmodule(tolua_S);
 tolua_cclass(tolua_S,"Class2","Class2","",NULL);
 tolua_beginmodule(tolua_S,"Class2");
 tolua_variable(tolua_S,"enu",tolua_get_Class2_Class2_enu,tolua_set_Class2_Class2_enu);
 tolua_function(tolua_S,"new",tolua_tclass_Class2_new00);
 tolua_endmodule(tolua_S);
#ifdef __cplusplus
 tolua_cclass(tolua_S,"V","V","",tolua_collect_V);
#else
 tolua_cclass(tolua_S,"V","V","",NULL);
#endif
 tolua_beginmodule(tolua_S,"V");
 tolua_function(tolua_S,"new",tolua_tclass_V_new00);
 tolua_function(tolua_S,"get",tolua_tclass_V_get00);
 tolua_endmodule(tolua_S);
#ifdef __cplusplus
 tolua_cclass(tolua_S,"C","C","",tolua_collect_C);
#else
 tolua_cclass(tolua_S,"C","C","",NULL);
#endif
 tolua_beginmodule(tolua_S,"C");
 tolua_function(tolua_S,"new",tolua_tclass_C_new00);
 tolua_function(tolua_S,"delete",tolua_tclass_C_delete00);
 tolua_function(tolua_S,"getV",tolua_tclass_C_getV00);
 tolua_function(tolua_S,"copyV",tolua_tclass_C_copyV00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
