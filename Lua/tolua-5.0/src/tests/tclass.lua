-- type convertion tests
assert(tolua.type(A.last) == 'Tst_A') -- first time the object is mapped
assert(tolua.type(B.last) == 'Tst_B') -- type convertion to specialized type
assert(tolua.type(A.last) == 'Tst_B') -- no convertion: obj already mapped as B


local a = A:new()
assert(tolua.type(A.last) == 'Tst_A') -- no type convertion: same type
local b = B:new()
assert(tolua.type(A.last) == 'Tst_B') -- no convertion: obj already mapped as B
local c = luaC:new(0)
assert(tolua.type(A.last) == 'Tst_C') -- no convertion: obj already mapped as C
assert(tolua.type(luaC.last) == 'Tst_C') 

local aa = A.AA:new()
local bb = A.BB:new()
local xx = create_aa()

-- method calling tests
assert(a:a() == 'A')
assert(b:a() == 'A')
assert(b:b() == 'B')
assert(c:a() == 'A')
assert(c:b() == 'B')
assert(c:c() == 'C')
assert(aa:aa() == 'AA')
assert(bb:aa() == bb:Base():aa())
assert(xx:aa() == 'AA')
assert(is_aa(bb) == true)

-- test nested enums
local a = Class2:new(Class.TWO)
assert(a.enu==Class.TWO)
a.enu = Class.ONE
assert(a.enu==Class.ONE)

-- test const types
local c1 = C:new(1)
local c2 = C:new(2)
local v = V:new(3)
local c1v = c1:getV()
local c2v = c2:getV()
local n1 = c1v:get()
local n2 = c2v:get()
local n3 = v:get()
assert(n1+n2==n3)

-- test ownershipping handling
-- should delete objects: 6 7 8 9 10 (it may vary!)
--[[
print('begin')
local set = {}
for i=1,10 do
 local c = luac:new(i)
	tolua.takeownership(c)
	set[i] = c
end
 
for i=1,5 do
 tolua.releaseownership(set[i])
end
for i=6,10 do
	set[i] = nil
end
collectgarbage()
collectgarbage()
collectgarbage()
print('end')
--]]

--[[
-- Test memory collection
for i=1,10 do
 local c = C:new(i)
	tolua.takeownership(c)
 local v = c:copyV() 
end
print('###########begin')
collectgarbage()
print('###########end')
--]]

print("Class test OK")
