#!/bin/bash

## Linux 32 bit 3.75

tar xzvf ../dists/fmodapi375linux.tar.gz

sudo mkdir -p /usr/local/include/fmod-3.75/fmod

( cd fmodapi375linux/api ; sudo cp inc/* /usr/local/include/fmod-3.75/fmod )
( cd fmodapi375linux/api ; sudo cp libfmod-3.75.so /usr/local/lib/  )

sudo ldconfig
