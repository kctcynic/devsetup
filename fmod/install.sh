#!/bin/bash

## Linux 32 bit 4.40

tar xzvf ../dists/fmodapi44004linux.tar.gz

sudo mkdir -p /usr/local/include/fmod-4.40

( cd fmodapi44004linux/api ; sudo cp inc/* /usr/local/include/fmod-4.40 )
( cd fmodapi44004linux/api/lib ; tar cf - . | (cd /usr/local/lib ; sudo tar xvf -  ) )

( cd fmodapi44004linux/fmoddesignerapi/api ; sudo cp inc/* /usr/local/include/fmod-4.40 )
( cd fmodapi44004linux/fmoddesignerapi/api/lib ; tar cf - . | (cd /usr/local/lib ; sudo tar xvf -  ) )


sudo ldconfig
