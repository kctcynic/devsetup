#!/bin/bash -f


mkdir -p ~/Software/Ogre
cd ~/Software/Ogre
hg clone https://bitbucket.org/sinbad/ogre src
mkdir build
cd build ; cmake -DOGRE_STATIC=1 -DCMAKE_BUILD_TYPE=Debug -G "Unix Makefiles" ../src

mkdir -p ~/Software/ffmpeg
cd ~/Software/ffmpeg
git clone git://source.ffmpeg.org/ffmpeg.git src
mkdir build
cd build ; ../src/configure

mkdir -p ~/Software/LibRocket
cd ~/Software/LibRocket
unzip ../dists/librocket_generic-source-1.2.1.zip
mkdir build
cd build
cmake -DBUILD_SHARED_LIBS=0 -DCMAKE_BUILD_TYPE=Debug ../libRocket/Build

