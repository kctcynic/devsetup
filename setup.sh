#!/bin/bash -f


### get all the source control packages
sudo apt-get -y install git mercurial subversion

### get the stuff needed to build kernels
sudo apt-get -y install build-essential

### get extra build tools
sudo apt-get -y install yasm cmake-qt-gui

### Needed by eclipse
sudo apt-get -y install openjdk-7-jdk gtk2-engines-pixbuf


sudo apt-get -y install libsdl1.2-dev


sudo apt-get -y install libreadline6-dev zlib1g-dev libxaw7-dev freeglut3-dev libxrandr-dev


sudo apt-get -y install libboost1.48-all-dev

# sudo apt-get -y install libtolua-dev libtolua++5.1-dev

sudo apt-get -y install libois-dev libfreetype6-dev libfreeimage-dev

sudo apt-get -y install libzzip-dev

##################  Library dependencies

sudo apt-get -y install libmysqlclient-dev libmysql++-dev

sudo apt-get -y install libcurl4-gnutls-dev libcrypto++-dev libxerces-c-dev

sudo apt-get -y install libsdl-mixer1.2-dev libsdl-image1.2-dev 

sudo apt-get -y install libgconf2-dev

sudo apt-get -y install libgle3-dev

sudo apt-get -y install nvidia-cg-toolkit

