#
# make sure to do the following first...
#
#  sh setup,sh
#  sh fetch.sh
#

all:
	cd ffmpeg/build; make
	cd glpng; make
	cd Lua; make
	cd Ogre/build ; make
	cd LibRocket/build ; make

install:
	cd ffmpeg/build; make install
	cd fmod; sh install375.sh
	cd glpng; make install
	cd Lua; make install
	cd Ogre/build ; make install
	cd LibRocket/build ; make install


